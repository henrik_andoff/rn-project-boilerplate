# RN Project Boilerplate
This repo is a boilerplate for a React Native project. For simplicity, there are just three scenes, with a bare bones UI. 

| Intro Slider                                    | Login Scene                             | Main Scene                            |
| ----------------------------------------------- |:---------------------------------------:| --------------------------------------|
| ![Intro Slider](screenshots/1-intro-slider.png) | ![Login Scene](screenshots/2-login.png) | ![Main Scene](screenshots/3-main.png) |

## Prerequesites
First install Xcode from the Mac app store and enable command line tools. Also consider installing React Native Debugger (https://github.com/jhen0409/react-native-debugger).

Then, assuming you are on a mac, do the do the following:

```
> brew install node
```

```
> brew install watchman
```

```
> npm install -g react-native-cli
```

## Clone Repository

```
> git clone https://henrik_andoff@bitbucket.org/henrik_andoff/rn-project-boilerplate.git
> cd rn-project-boilerplate
``` 

## Install & Link

```
> npm install
> react-native link
``` 

## Build & Run
The below command normally builds and runs the project in the simulator, so go ahead and do so. However, if it is the first time you are running the react native simulator you might need to create an empty project elsewhere, and build that first. I do not know why.

```
> react-native run-ios
``` 
Be aware that a clean build takes a long time.

## Included Features
This boilerplate lacks remote rest calls, and remote storage sync. I recommend using axios for rest calls, and I also recommend checking out Firebase for remote storage sync. 

* **Directory Structure**
  
  Source directory with sub directories for actions, reducers, components, scenes, locales, ...
  
* **React Native Debugger**

  Support for react native debugger is enabled by 'redux-devtools-extension'. See composeWithDevTools in `/src/App.js`.

* **Redux**
  
  Global state managed by redux. See `/src/App.js` for setup.
  
* **Local Storage**

  One property in the redux state, called boot, is persisted locally using `redux-persist`. See `/src/App.js` for persistance configuration with blacklisting of specific prop. 
  
* **Thunk**

  Thunk enables asyncronous action creators, and dispatching multiple actions from one action creator. See `/src/App.js` for setup.

* **Internationalization (i18n)**

  I18n is achieved using `i18n-js`. See `/src/I18n.js` for how it is setup. Note the use of `react-native-device-info` to check user's system locale. Translation files are located in `/src/locales`. See `/src/components/LoginForm.js` for example of how I18n is used.
  
* **Navigation**

  In app navigation is handled by `react-native-router-flux`. All scenes are declared in `/src/Router.js`, and this router component is activated in `/src/App.js`.
  
* **Startup State Machine**

  There are certain conditions that need to be checked every time the user activates the app (do we need to migrate a local DB after an app update, have we re-gained network access, have something happed on a remote server, ...). For this reason, each time the app is started, or is reactivated, it runs through a state machine. The state machine is implemented in the `IntroScene#componentDidUpdate()` in `/src/scenes/IntroScene.js`. The state machine also needs to run when the app is woken up from inactive. That behaviour is set up using a local state in `/src/Router.js`, see `RouterComponent#componentWillMount()`.

* **Intro Slider**

  At first startup the user is presented with an introduction slide show. The slide show is implemented using `react-native-app-intro-slider` in `/src/scenes/IntroSlider.js`.
  
* **GUI Components**

  The directory `/src/components/common` includes some reusable GUI components implemented from scratch. There are also many component libraries available as open source, like `react-native-elements`, but this project does not make use of those. 
