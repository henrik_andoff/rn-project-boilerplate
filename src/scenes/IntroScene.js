import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import AppIntroSlider from 'react-native-app-intro-slider';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import I18n from '../I18n';
import { colors } from '../resources/styles';
import * as types from '../actions/BootTypes';
import { transitionState, introSliderDisplayed } from '../actions/BootActions';
import { Spinner } from '../components/common';

class IntroScene extends Component {

    componentDidMount() {
        if (this.props.boot.state === types.BOOTSTATE_FINISHED) {
            this.props.transitionState({ state: types.BOOTSTATE_FINISHED });
        } else if (this.props.boot.introSliderDisplayed) {
            this.props.transitionState({ state: this.props.boot.state });
        } else {
            this.props.transitionState({ state: types.BOOTSTATE_FIRST_START });
        }
    }

    componentDidUpdate() {
        console.log('--->>> IntroScene#componentDidUpdate', this.props.boot);

        // Dummy state machine
        switch (this.props.boot.state) {

            case types.BOOTSTATE_FIRST_START:
                console.log('  BOOTSTATE_FIRST_START', types.BOOTSTATE_FIRST_RUNNING);
                this.props.transitionState({ state: types.BOOTSTATE_FIRST_RUNNING });
            break;
            case types.BOOTSTATE_FIRST_RUNNING:
                console.log('  BOOTSTATE_FIRST_RUNNING');
                this.props.transitionState({ state: types.BOOTSTATE_FIRST_END });
            break;
            case types.BOOTSTATE_FIRST_END:
                console.log('  BOOTSTATE_FIRST_END');
                this.props.transitionState({ state: types.SHOW_INTRO_SLIDER });
            break;

            case types.BOOTSTATE_SECOND_START:
                console.log('  BOOTSTATE_SECOND_START');
                this.props.transitionState({ state: types.BOOTSTATE_SECOND_RUNNING });
            break;
            case types.BOOTSTATE_SECOND_RUNNING:
                console.log('  BOOTSTATE_SECOND_RUNNING');
                this.props.transitionState({ state: types.BOOTSTATE_SECOND_END });
            break;
            case types.BOOTSTATE_SECOND_END:
                console.log('  BOOTSTATE_SECOND_END');
                this.props.transitionState({ state: types.BOOTSTATE_FINISHED });
            break;

            case types.BOOTSTATE_FINISHED:
                console.log('  BOOTSTATE_FINISHED');
                Actions.reset('login');
            break;

            default:
            break;
        }
    }

    onDone = () => {
        this.props.introSliderDisplayed({ isDisplayed: true });
        this.props.transitionState({ state: types.BOOTSTATE_SECOND_START });
    }

    renderIcon = (slideKey) => {
        switch (slideKey) {
            case 'first':
                return (<FontAwesome style={{ backgroundColor: 'transparent' }} name='cloud' size={150} color="white" />);
            case 'second':
                return (<FontAwesome style={{ backgroundColor: 'transparent' }} name='bomb' size={150} color="white" />);
            default:
                return (<FontAwesome style={{ backgroundColor: 'transparent' }} name='cloud' size={150} color="white" />);
        }
    }

    renderItem = props => (
        <View 
            style={[styles.mainContent, {
                backgroundColor: props.backgroundColor,
                paddingTop: props.topSpacer,
                paddingBottom: props.bottomSpacer,
                width: props.width,
                height: props.height,
            }]}
        >
            {this.renderIcon(props.key)}
            <View>
                <Text style={styles.title}>{props.title}</Text>
                <Text style={styles.text}>{props.text}</Text>
            </View>
        </View>
    );

    renderNextButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Ionicons
              name="md-arrow-round-forward"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
    }

    renderDoneButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Ionicons
              name="md-checkmark"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
    }

    render() {
        if (this.props.boot.state === types.SHOW_INTRO_SLIDER) {
            return (
                <AppIntroSlider 
                    slides={slides} 
                    renderItem={this.renderItem}
                    renderDoneButton={this.renderDoneButton}
                    renderNextButton={this.renderNextButton}
                    onDone={this.onDone} 
                />
            );
        } 

        return (
            <Spinner style={styles.emptyScreen} />
        );
    }
}

const styles = {
    emptyScreen: {
        flex: 1,
        backgroundColor: 'rgba(111, 84, 153)'
    },
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    text: {
        color: 'rgba(255, 255, 255, 0.8)',
        backgroundColor: 'transparent',
        textAlign: 'center',
        paddingHorizontal: 16,
    },
    title: {
        fontSize: 22,
        color: 'white',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    },
    image: {
      width: 160,
      height: 160,
    },
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    }
};

const slides = [
    {
        key: 'first',
        title: I18n.t('intro.slide1.title'),
        text: I18n.t('intro.slide1.text'),
        imageStyle: styles.image,
        backgroundColor: colors.primary,
    },
    {
        key: 'second',
        title: I18n.t('intro.slide2.title'),
        text: I18n.t('intro.slide2.text'),
        imageStyle: styles.image,
        backgroundColor: colors.primary,
    }
];

const mapStateToProps = (state) => {
    return {
        boot: state.boot,
    };
};

export default connect(mapStateToProps, { introSliderDisplayed, transitionState })(IntroScene);
