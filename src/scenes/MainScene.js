import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import { Card, CardSection } from '../components/common';

class MainScene extends Component {
    render() {
        return (
            <Card>
                <CardSection>
                    <View style={styles.viewStyle}>
                        <Text style={styles.textStyle}>
                            {this.props.user.email}
                        </Text>
                    </View>
                </CardSection>
            </Card>
        );
    }
}

const styles = {
    viewStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle: {
        fontSize: 28,
        fontWeight: 'bold'
    }
};

const mapStateToProps = state => {
    return {
        user: state.auth.user
    };
};

export default connect(mapStateToProps)(MainScene);
