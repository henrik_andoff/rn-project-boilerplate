import i18n from 'i18n-js';
import DeviceInfo from 'react-native-device-info';

import en from './locales/en.json';
import sv from './locales/sv.json';

i18n.defaultLocale = 'en';
i18n.locale = DeviceInfo.getDeviceLocale();
i18n.fallbacks = true;
i18n.translations = { en, sv };

export default i18n;
