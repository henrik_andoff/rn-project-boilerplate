import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';

import persistedRootReducer from './reducers';
import Router from './Router';

const App = () => {
    const INITIAL_STATE = {
        boot: {
            introSliderDisplayed: null,
            state: null
        }
    };

    const store = createStore(
        persistedRootReducer, 
        INITIAL_STATE, 
        composeWithDevTools(applyMiddleware(ReduxThunk))
    );
    const persistor = persistStore(store);

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <Router />
            </PersistGate>
        </Provider>
    );
};

export default App;
