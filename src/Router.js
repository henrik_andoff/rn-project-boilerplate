import React, { Component } from 'react';
import { AppState } from 'react-native';
import { connect } from 'react-redux';
import { Router, Scene, Stack } from 'react-native-router-flux';
import I18n from './I18n';
import IntroScene from './scenes/IntroScene';
import LoginScene from './scenes/LoginScene';
import MainScene from './scenes/MainScene';
import { transitionState } from './actions/BootActions';
import { BOOTSTATE_SECOND_START } from './actions/BootTypes';

class RouterComponent extends Component {

    state = {
        appState: AppState.currentState
    }

    componentWillMount() {
        AppState.addEventListener('change', this.handleAppStateChange);
    }

    shouldComponentUpdate() { 
        return false; // This prevents navigating to default when system dialog shown
    } 

    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
    }

    handleAppStateChange = (nextAppState) => {
        console.log('###>>> ', this.state, nextAppState);
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.props.transitionState({ state: BOOTSTATE_SECOND_START });
        }
        
        this.setState({ appState: nextAppState });
    }

    render() {
        return (
            <Router>
                <Stack key="root">
                    <Scene 
                        key="intro" 
                        component={IntroScene} 
                        hideNavBar
                        default
                    />
                    <Scene 
                        key="login" 
                        component={LoginScene} 
                        title={I18n.t('router.login')}    
                    />    
                    <Scene 
                        key="main" 
                        component={MainScene} 
                        title={I18n.t('router.welcome')}
                    />
                </Stack>
            </Router>
        );
    }
}

export default connect(null, { transitionState })(RouterComponent);
