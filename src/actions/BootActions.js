import { BSM_SET_STATE, INTRO_SLIDER_DISPLAYED } from './BootTypes';

export const transitionState = ({ state }) => {
    return (dispatch) => {
        dispatch({
            type: BSM_SET_STATE,
            payload: state
        });
    };
};

export const introSliderDisplayed = ({ isDisplayed }) => { 
    return (dispatch) => {
        dispatch({
            type: INTRO_SLIDER_DISPLAYED,
            payload: isDisplayed
        });
    };
};
