import * as types from '../actions/BootTypes';

const INITIAL_STATE = { 
    introSliderDisplayed: null,
    state: null
};

export default (state = INITIAL_STATE, action) => {
    console.log('--->>> BootStateMachineReducer action.payload', action.type, action.payload);
    switch (action.type) {
        case types.BSM_SET_STATE:
            return { 
                ...state,
                state: action.payload
            };

        case types.INTRO_SLIDER_DISPLAYED:
            return { 
                ...state,
                introSliderDisplayed: action.payload
            };
            
        default:
            return state;
    }
};
