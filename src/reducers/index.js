import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import AuthReducer from './AuthReducer';
import BootReducer from './BootReducer';

const rootPersistConfig = {
    key: 'root',
    storage,
    blacklist: ['auth', 'boot']
};

const bootPersistConfig = {
    key: 'boot',
    storage,
    blacklist: ['state']
};

const rootReducer = combineReducers({
    auth: AuthReducer,
    boot: persistReducer(bootPersistConfig, BootReducer)
});

const persistedReducer = persistReducer(rootPersistConfig, rootReducer);

export default persistedReducer;
